<!--
 * @Descripttion: 文档总说明
 * @version:
 * @Author: lhl
 * @Date: 2022-10-31 11:57:18
 * @LastEditors: lhl
 * @LastEditTime: 2022-11-01 09:38:16
-->
<div align="center">
  <img width="200" src="http://xxpromise.gitee.io/webpack5-docs/imgs/logo.svg">
  <h1>Webpack5 教程文档</h1>
  <h1>鱼樱前端</h1>
</div>

---

## 🎉 内容

- 👏 基础配置
- 💅 高级优化
- 🚀 项目配置
- 💪 深入原理

## 📦️ 启动方式

### 1. 下载依赖

```
npm i
```

### 2. 运行指令

- 开发环境启动项目：`npm start` 或 `npm run dev`
- 生产环境打包项目：`npm run build`

** 特别注意：启动文档所有目录不能有任何中文，否则会报错！**
** 该 vuepress 版本为 1.x 版本 2.x-beta 版本会在后面更新**
