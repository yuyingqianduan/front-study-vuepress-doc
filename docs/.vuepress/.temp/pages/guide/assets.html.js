export const data = {
  "key": "v-3c32c2ca",
  "path": "/guide/assets.html",
  "title": "",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 3,
      "title": "静态资源存放目录",
      "slug": "静态资源存放目录",
      "children": []
    }
  ],
  "git": {
    "contributors": []
  },
  "filePathRelative": "guide/assets.md"
}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
