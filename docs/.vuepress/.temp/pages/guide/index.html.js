export const data = {
  "key": "v-fffb8e28",
  "path": "/guide/",
  "title": "front-study-vuepress-doc",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 3,
      "title": "vuepress 官网",
      "slug": "vuepress-官网",
      "children": []
    },
    {
      "level": 3,
      "title": "搭建流程 就是 123",
      "slug": "搭建流程-就是-123",
      "children": []
    },
    {
      "level": 3,
      "title": "推荐主题",
      "slug": "推荐主题",
      "children": []
    },
    {
      "level": 3,
      "title": "添加忽略文件 到 .gitignore 主要是垃圾文件【或者临时生成】没必要上传到 git",
      "slug": "添加忽略文件-到-gitignore-主要是垃圾文件【或者临时生成】没必要上传到-git",
      "children": []
    }
  ],
  "git": {
    "contributors": []
  },
  "filePathRelative": "guide/README.md"
}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
