export const data = {
  "key": "v-2e43c986",
  "path": "/vue2Study/base.html",
  "title": "",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 3,
      "title": "基础语法",
      "slug": "基础语法",
      "children": []
    }
  ],
  "git": {
    "contributors": []
  },
  "filePathRelative": "vue2Study/base.md"
}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
