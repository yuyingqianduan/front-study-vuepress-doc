export const data = {
  "key": "v-12410dcb",
  "path": "/vue2Study/",
  "title": "",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 3,
      "title": "vue2 版本学习",
      "slug": "vue2-版本学习",
      "children": []
    }
  ],
  "git": {
    "contributors": []
  },
  "filePathRelative": "vue2Study/README.md"
}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
