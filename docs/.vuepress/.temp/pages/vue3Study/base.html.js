export const data = {
  "key": "v-6b200dc8",
  "path": "/vue3Study/base.html",
  "title": "",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 3,
      "title": "基础语法",
      "slug": "基础语法",
      "children": []
    }
  ],
  "git": {
    "contributors": []
  },
  "filePathRelative": "vue3Study/base.md"
}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
