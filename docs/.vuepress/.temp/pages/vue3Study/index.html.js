export const data = {
  "key": "v-4727490c",
  "path": "/vue3Study/",
  "title": "",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 3,
      "title": "vue3 版本学习",
      "slug": "vue3-版本学习",
      "children": []
    }
  ],
  "git": {
    "contributors": []
  },
  "filePathRelative": "vue3Study/README.md"
}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
